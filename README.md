# THEMIS Demo Repository

This repository is intended to provide all necessary files for the [demo presentation that will be presented at ISSTA'17](http://conf.researchr.org/event/issta-2017/issta-2017-demos-themis-a-tool-for-decentralized-monitoring-algorithms)

## Demonstration Video

The demo video can be found on youtube

[![Tutorial Video](https://img.youtube.com/vi/bKzLQ5X-J3w/0.jpg)](https://www.youtube.com/watch?v=bKzLQ5X-J3w)

## Setup

### Demonstration

* Clone the repository
* Extract the compressed files 
    * `unzip traces.zip`
    * `(cd plot/bench && unzip run.zip)`

### THEMIS

The demonstration assumes that THEMIS is installed and running, for instructions on how to do so, [check the main repository](https://gitlab.inria.fr/monitoring/themis).


**Note:** *It is recommended to use the [docker image](https://gitlab.inria.fr/monitoring/themis/tree/master/docker) and simply place all files in the docker mount point*


## About THEMIS

### THEMIS Abstract

THEMIS is a tool to facilitate the design, development, and analysis of decentralized monitoring algorithms; developed using Java and AspectJ. It consists of a library and command-line tools. THEMIS provides an API, data structures and measures for decentralized monitoring. These building blocks can be reused or extended to modify existing algorithms, design new more intricate algorithms, and elaborate new approaches to assess existing algorithms. We illustrate the usage of THEMIS by comparing two variants of a monitoring algorithm.

### Main Repository

* **[THEMIS Artifact Repository](https://gitlab.inria.fr/monitoring/themis)**: contains the main artifacts for the ISSTA papers, it contains longer experiments, source code and more details on using THEMIS

### Related Papers
* Antoine El-Hokayem and Ylies Falcone. 2017. Monitoring Decentralized Specifications. In Proceedings of the 26th International Symposium on Softare Testing and Analysis, ISSTA 2017, Santa-Barbara, USA, July 10-14, 2017. To appear.
* Antoine El-Hokayem and Ylies Falcone. 2017. THEMIS: A Tool for Decentralized Monitoring Algorithms. In Proceedings of the 26th International Symposium on Softare Testing and Analysis, ISSTA 2017, Santa-Barbara, USA, July 10-14, 2017. To appear.

