DB  	?= run.db
out		?= data.dat
rplot ?= plot.r 
Q     ?= query.sql

.PHONY: all gendata plot clean cleanall

all: plot
	
gendata: 
	 sqlite3 -header -csv ${DB} "`cat ${Q}`" > ${out}

plot:
	Rscript ${rplot}

clean:
	rm -f *.pdf

cleanall: 
	rm ${out}
