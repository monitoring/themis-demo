select alg, ltl_hash, ltl_depth, ltl_comp, 
      avg(msg_data) as "msg_data", 
      avg(msg_num) as "msg_num",
      avg(case when verdict == "NA" then run_len -10 else run_len end) as "run_len",
      avg(exp_avg) as "exp_avg", 
      avg(simp_avg) as "simp_avg", 
      avg(simp_total) as "simp_total", 
      avg(spread_simp) as "spread_simp",
      avg(max_delay) as "max_delay", 
      avg(resolutions) as "resolutions",
      avg(aut_states) as "aut_states",
      avg(aut_pairsmax) as "aut_pairs",
      avg(aut_labelmax) as "aut_label"
  FROM bench
  GROUP BY alg, ltl_hash, ltl_depth, ltl_comp
