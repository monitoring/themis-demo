library("xtable")
source("../common.r")

dat<-read.csv("data.dat", header = TRUE)
norm=dat$run_len
# Normalizing
dat$msg_num   = dat$msg_num /norm
dat$msg_data  = dat$msg_data/norm
dat$simp_total  = dat$simp_total/norm
dat$simp_avg    = dat$simp_avg/norm
dat$spread_simp = dat$spread_simp/norm


clbl <- "# Components"
names(dat)
# Number of Messages
th_bench(dat, "ltl_comp", "msg_num", clbl, "Number of Messages/Round", 2)
# Size of Messages
th_bench(dat, "ltl_comp", "msg_data", clbl,  "Data Transfered/Round", 11)
# Delay
th_bench(dat, "ltl_comp", "max_delay", clbl, "Maximum Delay", 2)
# Delay
th_bench(dat[dat$ltl_comp==3,], "ltl_depth", "max_delay", "Network Depth", "Maximum Delay")
# Comp
th_bench(dat, "ltl_comp", "simp_avg", clbl, "Maximum Simplifications/Monitor/Round")
# Comp
th_bench(dat, "ltl_comp", "simp_total", clbl, "Total Simplifications/Round")

mkcls <- function(x) {
  x = floor(x)
  y = x + 1
  return(paste("[", x,", ", y, "[", sep=""))
}

dat['spec'] = dat$aut_label * dat$aut_pairs 
dat['del']  = as.factor(sapply(dat$max_delay, mkcls))
th_group(dat[dat$alg=="MigrationRR",], "del", "Delay", "spec", "msg_data", "LP", "Data Transfered/Round") +
    theme(legend.position="bottom")

attach(dat)
davg <-aggregate(dat, by=list(alg),  FUN=mean, na.rm=TRUE)
detach(dat)

names(davg)
dfill=davg[c(1, 11, 10)]
dfill.long<-melt(dfill)

gdraw(ggplot(dfill.long, aes(Group.1,value,fill=variable)) + geom_bar(stat="identity", position="dodge") +
  ylab("Simplifications") +
  xlab("Algorithm") +
  scale_fill_discrete("",labels=c("Total/Round", "Max/Monitor/Round"))) +
  theme(legend.position="top")
ggsave("conv.pdf")

attach(davg)
gdraw(ggplot(data=davg, aes_string(x="Group.1", y="spread_simp")) + 
  geom_bar(width=.5, aes(fill=Group.1),stat="identity") + ylab("Convergence") + xlab("Algorithm")) +
  theme(legend.position="none")
ggsave("convergence.pdf")

names(davg)
print(xtable(davg[c(1,7,6,10,11,12,13)]), include.rownames=FALSE)

detach(davg)
attach(dat)
names(dat)
f<-mean
dres<-dat %>%
    group_by(alg, ltl_comp) %>%
    summarise(delay=f(max_delay), num=f(msg_num), data=f(msg_data), simpt=f(simp_total), simpm=f(simp_avg), conv=f(spread_simp)) %>% as.data.frame
dres
print(xtable(dres), include.rownames=FALSE)
