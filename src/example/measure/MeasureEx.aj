package example.measure;

import anon.themis.measurements.Instrumentation;
import anon.themis.monitoring.MonitoringAlgorithm;

import anon.themis.inference.Memory;
import anon.themis.measurements.Measure;
import anon.themis.measurements.Measures;

public aspect MeasureEx extends Instrumentation {

	protected void setupOnce(MonitoringAlgorithm alg) {
	};

	protected void setupRun(MonitoringAlgorithm alg) {
		setDescription("Example Measure");
		addMeasure(new Measure("ex_memory", "Memory Access",  0, Measures.addInt));
	};

	after() : call(* Memory.get(..)) {
		update("ex_memory", 1);
	}
}
