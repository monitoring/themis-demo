package example.alg;


import java.util.HashMap;
import java.util.Map;

import anon.themis.monitoring.Component;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.Monitor;
import anon.themis.monitoring.MonitorSettings;
import anon.themis.monitoring.MonitoringAlgorithm;
import anon.themis.monitoring.ReportVerdict;

import anon.themis.monitoring.SpecAutomata;	// Automata Specifications
import anon.themis.utils.Convert;					  // Spec converter
import anon.themis.utils.SimplifierFactory; // Simplifiers
import anon.themis.inference.Representation;// EHE datastruct

public class MyAlgorithm extends MonitoringAlgorithm {
	public MyAlgorithm() {
		super();
	}
	public MyAlgorithm(MonitorSettings config) {
		super(config);
	}

	protected Map<Integer, ? extends Monitor> setup()  {
		//manage Spec
		SpecAutomata spec  = Convert.makeAutomataSpec(config.getSpec().get("root"));
		Representation ehe = new Representation(spec.getAut(), SimplifierFactory.getDefault());

		int c = config.getComponentsSize();

		Map<Integer, Monitor> mons 	= new HashMap<Integer, Monitor>(c);

		Monitor main = new Main(0, ehe);
		//Main monitor is ID = 0
		mons.put(0, main);

		int i = 0;
		//Create one slave per component
		for(Component comp : config.getComponents()) {
			if(i == 0) {
				attachMonitor(comp, main);
				i++;
				continue;
			}
			Monitor mon = new Forward(i);
			attachMonitor(comp, mon);
			mons.put(i,  mon);
			i++;
		}

		return mons;
	}

	@Override
	public void report(ReportVerdict v) {
		System.out.println("[Verdict] " + v);
	}

	@Override
	public void abort(ExceptionStopMonitoring error) {
		System.err.println("[Abort] " + error.getError());
	}

}
