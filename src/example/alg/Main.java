package example.alg;

import anon.themis.automata.Atom;
import anon.themis.inference.Memory;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.GeneralMonitor;
import anon.themis.monitoring.ReportVerdict;

import anon.themis.comm.packets.MemoryPacket;
import anon.themis.comm.protocol.Message;
import anon.themis.automata.VerdictTimed;
import anon.themis.monitoring.SpecAutomata;
import anon.themis.inference.Representation; // EHE Datastructure
import anon.themis.inference.MemoryAtoms;

public class Main extends GeneralMonitor {
	// EHE to monitor
	private Representation ehe;
	// Memory to store observations
	private Memory<Atom> memory = new MemoryAtoms();

	public Main(int id, Representation ehe) {
		super(id);
		this.ehe = ehe;
	}

	@Override
	public void monitor(int timestamp, Memory<Atom> observations) throws ReportVerdict,
	ExceptionStopMonitoring {
		//Add current timestamp observations to memory
		memory.merge(observations);
		//Read Memory sent by other monitors
		Message m;
		while((m = recv()) != null) {
			MemoryPacket packet = (MemoryPacket) m;
			memory.merge((Memory<Atom>) packet.mem);
		}
		//Nothing observed: Either first timestamp or end of trace
		if(observations.isEmpty()) return;
		else						 ehe.tick(); //Move one timestamp in future
		// Attempt to find a verdict on EHE with current memory
		ehe.update(memory, -1);
		VerdictTimed v = ehe.scanVerdict();

		if(v.isFinal())
		throw new ReportVerdict(v.getVerdict(), timestamp);

		//Cleanup EHE
		ehe.dropResolved();
	}
	@Override
	public void reset() {
		memory.clear();
		ehe.reset(0);
	}
	@Override
	public void setup() {

	}
	@Override
	public void communicate() {

	}
}
