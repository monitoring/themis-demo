package example.alg;

import anon.themis.automata.Atom;
import anon.themis.inference.Memory;
import anon.themis.monitoring.ExceptionStopMonitoring;
import anon.themis.monitoring.GeneralMonitor;
import anon.themis.monitoring.ReportVerdict;

//Message to carry Memory
import anon.themis.comm.packets.MemoryPacket;

public class Forward extends GeneralMonitor {

	public Forward(int id) {
		super(id);
	}

	@Override
	public void monitor(int timestamp, Memory<Atom> observations) throws ReportVerdict,
	ExceptionStopMonitoring {
		//Send to mon 0
		send(0, new MemoryPacket(observations));
		System.out.printf("Monitor [%3d] @ %3d - %s\n",
		this.getID(), timestamp, observations.toString());
	}
	@Override
	public void reset() {
	}
	@Override
	public void setup() {
	}
	@Override
	public void communicate() {
	}
}
