# Path to the Themis JAR
THEMISPATH ?=../themis.jar
# Add classpath
JCP= ${CP}:${THEMISPATH}

# Java command
RUNXP=java -Xms8G -Xmx12G -cp ${JCP} -javaagent:${THEMISPATH} anon.themis.tools.experiment.Experiment

# Text containing formulae
FORMS ?= forms.txt

.PHONY: clean 

default:
	$(info Use 'run' to run experiment) 

# Cleanup by deleting spec 
clean:
	rm run.db old.run.db
# Run the experiment
run: 
	${RUNXP} .
