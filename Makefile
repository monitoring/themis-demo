build:
	mvn clean package

default:
	java -javaagent:themis.jar anon.themis.tools.Run \
		 -nr 110 -nc 3 -in traces -tid 0 -spec spec/spec1.xml \
		 -alg anon.themis.algorithms.migration.Migration

noagent:
	java -cp themis.jar anon.themis.tools.Run \
		 -nr 110 -nc 3 -in traces -tid 0 -spec spec/spec1.xml \
		 -alg anon.themis.algorithms.migration.Migration


example-noagent:
	java -cp target/example.jar:themis.jar anon.themis.tools.Run \
		 -nr 110 -nc 3 -in traces -tid 0 -spec spec/spec1.xml \
		 -alg example.alg.MyAlgorithm
example:
	make custom CP=target/example.jar ALG=example.alg.MyAlgorithm


sc1:
	make custom CP=conf/only:target/example.jar ALG=anon.themis.algorithms.migration.Migration

custom:
	java -cp ${CP}  -javaagent:themis.jar anon.themis.tools.Run \
		 -nr 110 -nc 3 -in traces -tid 0 -spec spec/spec1.xml \
		 -alg ${ALG}

docs:
	pandoc -c github.css README.md -o index.html
